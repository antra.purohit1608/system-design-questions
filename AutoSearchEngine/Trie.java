import java.util.*;
public class Trie {
    boolean isEnd;
    Trie[] tries;
    Trie() {
        this.isEnd = false;
        this.tries = new Trie[26];

    }
    public  void insert(String word) {
        Trie cur = this;
        for(int i=0;i<word.length();i++) {
            char c = word.charAt(i);
            if(cur.tries[c-'a']  == null) {
                cur.tries[c-'a'] = new Trie();

            }
            cur = cur.tries[c-'a'];

        }
        cur.isEnd = true;

    }
    public void dfs(Trie cur,ArrayList<String> ans,StringBuilder sb) {
        if(cur.isEnd == true) {
            ans.add(sb.toString());
            return;
        }
        for(int i=0;i<26;i++) {
            if(cur.tries[i] != null) {
                char c =(char) (i+'a');
                sb.append(c);
                dfs(cur.tries[i],ans ,sb);
                sb.setLength(sb.length()-1);
            }
        }
    }
    public List<String> find(String word,Trie root) {
        Trie cur = root;
        ArrayList<String> ans = new ArrayList<String>();
        for(int i=0;i<word.length();i++) {
            char c = word.charAt(i);
            if(cur.tries[c-'a']  == null) {
                return  ans;
            }
            cur = cur.tries[c-'a'];

        }
        StringBuilder sb = new StringBuilder();
        dfs(cur,ans,sb);
        return ans;
    }
    public List<List<String>>  autoSuggestion(String word) {
        ArrayList<List<String>> ans = new ArrayList<List<String>>();
        for(int i=0;i<word.length();i++) {
            String s = word.substring(0,i+1);
            List<String> as = find(s,this);
            ans.add(as);
        }
        return ans;
    }
}
